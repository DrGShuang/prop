package domini.controladors;

import domini.classes.Tauler;
import domini.shared.Color;
import domini.shared.Pair;
import java.util.Scanner;

import java.io.IOException;

import static domini.controladors.IO.print;

public class DriverTauler {

    public static void main(String[] args) throws IOException, CloneNotSupportedException {
        escullMetodeatestejar();
    }

    public static void escullMetodeatestejar() throws IOException, CloneNotSupportedException {
        System.out.println( "Selecciona el tipus de mètode a testejar:" );
        System.out.println( "1 -> Constructores" );
        System.out.println( "2 -> Gets" );
        System.out.println( "3 -> Sets" );
        System.out.println( "4 -> Altres" );

        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        switch(input) {
            case "1":
                constructores();
                break;
            case "2":
                escullget();
                break;
            case "3":
                escullset();
                break;
            case "4":
                escullaltres();
                break;
        }
    }

    public static void constructores() throws IOException {
        System.out.println( "Selecciona quina constructora testejar:" );
        System.out.println( "1 -> Constructora per defecte" );
        System.out.println( "2 -> Constructora per defecte amb paràmetres de les regles" );

        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        switch(input) {
            case "1":
                testConstructora1();
                break;
            case "2":
                testConstructora2();
        }
    }

    private static void escullget() throws IOException {
        System.out.println( "Selecciona quin dels gets testejar:" );
        System.out.println( "1 -> getIdtauler()" );
        System.out.println( "2 -> getFitxesB()" );
        System.out.println( "3 -> getFitxesN()" );
        System.out.println( "4 -> gettauler()" );
        System.out.println( "5 -> getHoritzontal()" );
        System.out.println( "6 -> getVertical()");
        System.out.println( "7 -> getDiagonal()");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        switch(input) {
            case "1":
                testgetIdentificador();
                break;
            case "2":
                testgetFitxesB();
                break;
            case "3":
                testgetFitxesN();
                break;
            case "4":
                testgettauler();
                break;
            case "5":
                testgetHoritzontal();
                break;
            case "6":
                testgetVertical();
                break;
            case "7":
                testgetDiagonal();
                break;
        }
    }

    private static void escullset() throws IOException {
        System.out.println( "Selecciona quin dels sets testejar:" );
        System.out.println( "1 -> settauler(CASELLA[][] taulernou)" );
        System.out.println( "2 -> setHoritzontal(boolean h)");
        System.out.println( "3 -> setVertical(boolean v)");
        System.out.println( "4 -> setDiagonal(boolean d)");
        System.out.println( "5 -> setIdTauler(int id)");
        System.out.println( "6 -> setFitxesB(int b)");
        System.out.println( "7 -> setFitxesN(int n)");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        switch(input) {
            case "1":
                testsettauler();
                break;
            case "2":
                testsetHoritzontal();
                break;
            case "3":
                testsetVertical();
                break;
            case "4":
                testsetDiagonal();
                break;
            case "5":
                testsetIdTauler();
                break;
            case "6":
                testsetFitxesB();
                break;
            case "7":
                testsetFitxesN();
                break;
        }
    }

    private static void escullaltres() throws IOException, CloneNotSupportedException {
        System.out.println( "Selecciona quin dels mètodes restants testejar:" );
        System.out.println( "0 -> Clone()");
        System.out.println( "1 -> posicioValida (Pair p, Color color)" );
        System.out.println( "2 -> hihamoviments(boolean negre)");
        System.out.println( "3 -> isacabat()" );
        System.out.println( "4 -> posarFitxa()" );
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        switch(input) {
            case "0":
                testclone();
                break;
            case "1":
                testsposicioValida();
                break;
            case "2":
                testhihamoviments();
                break;
            case "3":
                testsisacabat();
                break;
            case "4":
                testsposarFitxa();
                break;
        }
    }

    private static void testsposicioValida() throws IOException {
        System.out.println( "Selecciona quin dels test executar:" );
        System.out.println( "1 -> test1" );
        System.out.println( "2 -> test2" );
        System.out.println( "3 -> test3" );
        System.out.println( "4 -> test4" );
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        switch(input) {
            case "1":
                testposicioValida1();
                break;
            case "2":
                testposicioValida2();
                break;
            case "3":
                testposicioValida3();
                break;
            case "4":
                testposicioValida4();
                break;
        }
    }

    private static void testsisacabat() throws IOException {
        System.out.println( "Selecciona quin dels test executar:" );
        System.out.println( "1 -> test1" );
        System.out.println( "2 -> test2" );
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        switch(input) {
            case "1":
                testisacabat1();
                break;
            case "2":
                testisacabat2();
                break;
        }
    }

    private static void testsposarFitxa() throws IOException {
        System.out.println( "Selecciona quin dels test executar:" );
        System.out.println( "1 -> test1" );
        System.out.println( "2 -> test2" );
        System.out.println( "3 -> test3" );
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        switch(input) {
            case "1":
                testposarFitxa1();
                break;
            case "2":
                testposarFitxa2();
                break;
            case "3":
                testposarFitxa3();
                break;
        }
    }

    //constructores

    private static void testConstructora1() {// constructora per defecte
        Tauler t = new Tauler(1);
        System.out.println("El tauler té com a tauler");
        print(t.getTauler());
        if(t.getDiagonals() && t.getVertical() && t.getHoritzontal() && t.getFitxesN() == 2 && t.getFitxesB() == 2
                && t.getIdTauler() == 1) System.out.println("S'han creat bé els atributs tauler");
        else System.out.println("Ho sento, no s'ha creat bé els atributs de tauler");
    }

    private static void testConstructora2() { // amb paràmetres de les regles
        Tauler t = new Tauler(1, false, false, true);
        System.out.println("El tauler creat té com a tauler");
        print(t.getTauler());
        if(t.getDiagonals() && !t.getVertical() && !t.getHoritzontal() && t.getFitxesN() == 2 && t.getFitxesB() == 2
                && t.getIdTauler() == 1) System.out.println("S'han creat bé els atributs de tauler");
        else System.out.println("Ho sento, no s'ha creat bé els atributs de tauler");
    }

    // Tests gets
    private static void testgetIdentificador() {
        Tauler taulernou = new Tauler(15);
        System.out.println("El tauler té el número "+ taulernou.getIdTauler() + " com a identificador");
    }

    private static void testgetFitxesB() { //amb el tauler per defecte
        Tauler taulernou = new Tauler(1);
        System.out.println( "Hi ha "+ taulernou.getFitxesB() +" fitxes blanques");
    }

    private static void testgetFitxesN() { //amb el tauler per defecte
        Tauler taulernou = new Tauler(1);
        System.out.println( "Hi ha "+ taulernou.getFitxesN() +" fitxes negres");
    }

    private static void testgettauler() { //amb el tauler per defecte
        Tauler taulernou = new Tauler(1);
        print(taulernou.getTauler());
    }

    private static void testgetHoritzontal() {
        Tauler taulernou = new Tauler(1);
        if(taulernou.getHoritzontal()) System.out.println( "És possible fer captures en horitzontal");
        else System.out.println( "No és possible fer captures en horitzontal");
    }

    private static void testgetVertical() {
        Tauler taulernou = new Tauler(1);
        if(taulernou.getVertical()) System.out.println( "És possible fer captures en vertical");
        else System.out.println( "No és possible fer captures en vertical");
    }

    private static void testgetDiagonal() {
        Tauler taulernou = new Tauler(1);
        if(taulernou.getDiagonals()) System.out.println( "És possible fer captures en diagonal");
        else System.out.println( "No és possible fer captures en diagonal");
    }


    // Tests sets
    private static void testsettauler() throws IOException { // No és funcional
        Tauler taulernou = new Tauler(1);
        IO.llegeixTauler(taulernou, "jocsProves/Tauler/input_testsetTauler.txt");
        print(taulernou.getTauler());
    }

    private static void testsetHoritzontal() {
        Tauler taulernou = new Tauler(1);
        taulernou.setHoritzontal(false);
        if(taulernou.getHoritzontal()) System.out.println( "És possible fer captures en horitzontal");
        else System.out.println( "No és possible fer captures en horitzontal");
    }

    private static void testsetVertical() {
        Tauler taulernou = new Tauler(1);
        taulernou.setVertical(false);
        if(taulernou.getVertical()) System.out.println( "És possible fer captures en vertical");
        else System.out.println( "No és possible fer captures en vertical");
    }

    private static void testsetDiagonal() {
        Tauler taulernou = new Tauler(1);
        taulernou.setDiagonals(false);
        if(taulernou.getDiagonals()) System.out.println( "És possible fer captures en les diagonals");
        else System.out.println( "No és possible fer captures en les diagonals");
    }

    private static void testsetIdTauler() {
        Tauler taulernou = new Tauler(1);
        int num = 123;
        taulernou.setIdTauler(num);
        if(taulernou.getIdTauler() == num) System.out.println("La modificació ha estat realitzada correctament.");
        else System.out.println("Ho sento, la modificació no ha tingut èxit!");
    }

    private static void testsetFitxesB() {
        Tauler taulernou = new Tauler(1);
        taulernou.setFitxesB(30);
        if(taulernou.getFitxesB() == 30) System.out.println("La modificació ha estat realitzada correctament.");
        else System.out.println("Ho sento, la modificació no ha tingut èxit!");
    }

    private static void testsetFitxesN() {
        Tauler taulernou = new Tauler(1);
        taulernou.setFitxesN(30);
        if(taulernou.getFitxesN() == 30) System.out.println("La modificació ha estat realitzada correctament.");
        else System.out.println("Ho sento, la modificació no ha tingut èxit!");
    }


    // Tests altres

    private static void testclone() throws IOException, CloneNotSupportedException {
        Tauler tauler = new Tauler(1, true, false, true);
        tauler.setIdTauler(14);
        IO.llegeixTauler(tauler, "jocsProves/Tauler/input_test_clone.txt");
        Tauler nou = tauler.clone();
        print(nou.getTauler());
        if(nou.getHoritzontal() && !(nou.getVertical()) && nou.getDiagonals() && nou.getFitxesB() == 16 &&
                nou.getFitxesN() == 24 && nou.getIdTauler() == 14) System.out.println("La còpia s'ha efectuat correctament");
        else System.out.println("Ho sento, la còpia no s'ha efectuat correctament");
    }

    private static void testposicioValida1() { // Tauler per defecte
        Tauler tauler = new Tauler(1);
        Pair pos = new Pair(15,15);
        Color color = Color.Blanc;
        if(tauler.posicioValida(pos, color)) System.out.println("És una posició valida");
        else System.out.println("Ho sento, no és una posició valida");
    }

    private static void testposicioValida2() { // Tauler per defecte
        Tauler tauler = new Tauler(1);
        Pair pos = new Pair(3,3);
        Color color = Color.Negre;
        if(tauler.posicioValida(pos, color)) System.out.println("És una posició valida");
        else System.out.println("Ho sento, no és una posició valida");
    }

    private static void testposicioValida3() { // Tauler per defecte
        Tauler tauler = new Tauler(1);
        Pair pos = new Pair(2,2);
        Color color = Color.Blanc;
        if(tauler.posicioValida(pos, color)) System.out.println("És una posició valida");
        else System.out.println("Ho sento, no és una posició valida");
    }

    private static void testposicioValida4() { // Tauler per defecte
        Tauler tauler = new Tauler(1);
        Pair pos = new Pair(2,3);
        Color color = Color.Blanc;
        if(tauler.posicioValida(pos, color)) System.out.println("És una posició valida");
        else System.out.println("Ho sento, no és una posició valida");
    }

    private static void testhihamoviments() throws IOException {
        Tauler tauler = new Tauler(1);
        IO.llegeixTauler(tauler, "jocsProves/Tauler/input_test_hihamoviments.txt");
        if(!tauler.hihamoviments(true) && tauler.hihamoviments(false)) System.out.println("El test" +
                " ha sortit correctament");
        else System.out.println("Ho sento, el test no ha sortit com s'esperava");
    }

    private static void testisacabat1() {// Tauler per defecte
        Tauler tauler = new Tauler(1);
        if(tauler.isacabat()) System.out.println("Ja s'ha acabat la partida");
        else System.out.println("Encara es pot jugar");
    }

    private static void testisacabat2() throws IOException { // Tauler per defecte
        Tauler tauler = new Tauler(1, true, false, false);
        IO.llegeixTauler(tauler, "jocsProves/Tauler/input_test_isacabat2.txt");
        if(tauler.isacabat()) System.out.println("Ja s'ha acabat la partida");
        else System.out.println("Encara es pot jugar");
    }

    private static void testposarFitxa1() throws IOException {
        Tauler tauler = new Tauler(1);
        IO.llegeixTauler(tauler, "jocsProves/Tauler/input_test_posarfitxa1.txt");
        Pair pos = new Pair(3,4);
        Color col = Color.Blanc;
        tauler.posarFitxa(pos, col);
        print(tauler.getTauler());
        if(tauler.getFitxesB() == 28 && tauler.getFitxesN() == 0) System.out.println("Les fitxes s'han modificat correctament");
        else System.out.println("Compte, les fitxes no s'han modificat correctament.");
    }

    private static void testposarFitxa2() throws IOException {
        Tauler tauler = new Tauler(1);
        IO.llegeixTauler(tauler, "jocsProves/Tauler/input_test_posarfitxa2.txt");
        Pair pos = new Pair(3,4);
        Color col = Color.Blanc;
        tauler.posarFitxa(pos, col);
        print(tauler.getTauler());
        if(tauler.getFitxesB() == 8 && tauler.getFitxesN() == 10) System.out.println("Les fitxes s'han modificat correctament ");
        else System.out.println("Compte, les fitxes no s'han modificat correctament "  + tauler.getFitxesB() + " " + tauler.getFitxesN());
    }

    private static void testposarFitxa3() throws IOException {
        Tauler tauler = new Tauler(1);
        IO.llegeixTauler(tauler, "jocsProves/Tauler/input_test_posarfitxa3.txt");
        Pair pos = new Pair(3,4);
        Color col = Color.Negre;
        tauler.posarFitxa(pos, col);
        print(tauler.getTauler());
        if(tauler.getFitxesB() == 7 && tauler.getFitxesN() == 7) System.out.println("Les fitxes no s'han modificat");
        else System.out.println("Compte, les fitxes s'han modificat correctament "  + tauler.getFitxesB() + " " + tauler.getFitxesN());
    }
}

