package domini.controladors;

import java.io.IOException;
import java.util.Scanner;

public class PrincipalCtrl {

    public static void main(String[] args) throws IOException, CloneNotSupportedException {
        System.out.println("Hola, sóc el controlador principal!");
        escullControlador();
    }

    public static void escullControlador() throws IOException, CloneNotSupportedException {
        IO.print("Selecciona el controlador:");
        IO.print("1 -> Jugar partida");
        IO.print("2 -> Passar tests");

        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        switch (input) {
            case "1":
                PartidaCtrl.iniciarPartida();
                break;
            case "2":
                PrincipalCtrl.tests();
                break;
            default:
                System.out.println( "Si us plau, entra un valor de l'1 al 2" );
                escullControlador();
        }
        IO.print("\n");
        main(new String[]{""});
    }

    public static void tests() throws IOException, CloneNotSupportedException {
        IO.print("Selecciona la classe a testejar: ");
        IO.print("1 -> Casella");
        IO.print("2 -> Tauler");
        IO.print("3 -> Heurística");
        IO.print("4 -> Jugador");
        IO.print("5 -> Màquina");
        IO.print("6 -> Partida");
        IO.print("7 -> Perfil");
        IO.print("8 -> Estadístiques");
        IO.print("9 -> Rànquing");
        IO.print("10 -> Sortir");
        Scanner scanner = new Scanner(System.in);
        String[] arg = new String[] {""};
        String input = scanner.nextLine();
        switch (input) {
            case "1":
                CASELLAtests.main(arg);
                break;
            case "2":
                DriverTauler.main(arg);
                break;
            case "3":
                DriverHeuristica.main(arg);
                break;
            case "4":
                DriverJugador.main(arg);
                break;
            case "5":
                MaquinaTest.main(arg);
                break;
            case "6":
                PartidaTest.main(arg);
                break;
            case "7":
                DriverPerfil.main(arg);
                break;
            case "8":
                IO.print("Pendent d'integrar");
                break;
            case "9":
                IO.print("Pendent d'integrar");
                break;
            case "10":
                break;
        }
    }
}
