package domini.Test;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;


public class Suite {

    public static void main(String[] args) {
        Result result = JUnitCore.runClasses(RankingsTest.class);


        for (Failure failure : result.getFailures()) {
            System.out.println(failure.toString());
        }
        if (result.wasSuccessful()) {
            System.out.println("El test ha ido bien");
        } else {
            System.out.println("El test ha ido mal");
        }

        Result result1 = JUnitCore.runClasses(EstadistiquesTest.class);


        for (Failure failure : result1.getFailures()) {
            System.out.println(failure.toString());
        }
        if (result1.wasSuccessful()) {
            System.out.println("El test ha ido bien");
        } else {
            System.out.println("El test ha ido mal");
        }


    }

}
