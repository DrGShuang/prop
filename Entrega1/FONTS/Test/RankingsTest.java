package domini.Test;


import domini.classes.Maquina;
import domini.classes.Perfil;
import domini.classes.Rankings;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;


public class RankingsTest {


    private static Rankings rankings;
    @Test
    public void TestRankings() {
        rankings = new Rankings();
        Assert.assertNotNull(" Ranking General is null;", rankings.getrankingT());
        Assert.assertNotNull("Ranking Jugadors is null;", rankings.getrankingP());
        System.out.println("Instància creada");
    }




    @Test
    public  void testafegir_jugador() { //actualitzar puntuacio
        rankings = new Rankings();
        Perfil j1 = new Perfil("pepito");
        rankings.afegir_jugador(j1.getJugador());
        Assert.assertFalse(rankings.getrankingP().isEmpty()); //Comprovacio no esta buit
        Assert.assertFalse(rankings.getrankingT().isEmpty());
        Assert.assertEquals("S'ha afegit el jugador malament ;", rankings.getrankingP().size(), rankings.getrankingT().size());

        rankings = new Rankings();
        Maquina m1 = new Maquina("robot");
        rankings.afegir_jugador(m1.getJugador());
        Assert.assertTrue(rankings.getrankingP().isEmpty());
        Assert.assertFalse(rankings.getrankingT().isEmpty());
        Assert.assertNotEquals("S'ha afegit la maquina malament ;", rankings.getrankingP().size(), rankings.getrankingT().size());
    }

    @Test
    public void testactualitzar_puntuacio() {
        rankings = new Rankings();
        Perfil j1 = new Perfil("pepito");
        rankings.afegir_jugador(j1.getJugador());
        rankings.actualitzar_puntuacio(j1.getJugador());
        Assert.assertNotEquals("No concorda amb el resultat /n", java.util.Optional.ofNullable(rankings.getrankingP().get("Pepito")), 10.0);
        Assert.assertNotEquals("No concorda amb el resultat /n", java.util.Optional.ofNullable(rankings.getrankingT().get("Pepito")), 10.0);
    }

    @Test
    public void test_show_map(){
        rankings = new Rankings();

        Perfil j1 = new Perfil("pepito");
        j1.setPuntuacio_ranking(3);
        rankings.afegir_jugador(j1.getJugador());

        Perfil j2 = new Perfil("jhamir");
        j2.setPuntuacio_ranking(7);
        rankings.afegir_jugador(j2.getJugador());

        Perfil j3 = new Perfil("pipo");
        j3.setPuntuacio_ranking(4);
        rankings.afegir_jugador(j3.getJugador());

        rankings.actualitzar_puntuacio(j1.getJugador());
        rankings.actualitzar_puntuacio(j2.getJugador());
        rankings.actualitzar_puntuacio(j3.getJugador());

        System.out.println(rankings.getrankingT().size());
        for(String keys: rankings.getrankingT().keySet()){
            System.out.println(keys + " " + rankings.getrankingP().get(keys)) ;
        }

    }
}
