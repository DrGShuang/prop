package domini.classes;

import domini.shared.Color;
import domini.shared.Pair;

import java.util.LinkedList;


public class Heuristica {
    private static final int[][] pesos = {
            { 4, -3,  2,  2,  2,  2, -3, 4},
            {-3, -4, -1, -1, -1, -1, -4,-3},
            { 2, -1,  1,  0,  0,  1, -1, 2},
            { 2, -1,  0,  1,  1,  0, -1, 2},
            { 2, -1,  0,  1,  1,  0, -1, 2},
            { 2, -1,  1,  0,  0,  1, -1, 2},
            {-3, -4, -1, -1, -1, -1, -4,-3},
            { 4, -3,  2,  2,  2,  2, -3, 4}
    };
    private static final Pair[] direccions = {
            new Pair(-1, 0),
            new Pair(-1, 1),
            new Pair( 0, 1),
            new Pair( 1, 1),
            new Pair( 1, 0),
            new Pair( 1,-1),
            new Pair( 0,-1),
            new Pair(-1,-1)};

    private enum gamePhase {
        earlyGame,
        midGame,
        lateGame
    }

    /**
     * Funció per saber el color de l'oponent
     * @param player Color del jugador actual
     * @return Color del jugador oponent
     */
    private static Color oponent(Color player) {
        Color opnt;
        if (player == Color.Blanc) opnt = Color.Negre;
        else opnt = Color.Blanc;
        return opnt;
    }

    /**
     * Avaluació segons els corners
     * @param board Estat actual del taulell
     * @param player Color del jugador actual
     * @return Diferencia entre #corners dels dos jugadors
     */
    private static int corners(Tauler board, Color player) {
        int minp = 0;
        int maxp = 0;

        boolean corner00, corner07, corner70, corner77;
        for (int i = 0; i < 8; i++){
            for (int j = 0; j < 8; j++){
                corner00 = (i == 0 && j == 0);
                corner07 = (i == 0 && j == 7);
                corner70 = (i == 7 && j == 0);
                corner77 = (i == 7 && j == 7);

                if (corner00 || corner07 || corner70 || corner77){
                    if (board.getTauler()[i][j].getcolor() == player) minp += 1;
                    if (board.getTauler()[i][j].getcolor() == oponent(player)) maxp += 1;
                }
            }
        }
        return (minp - maxp);
    }

    /**
     * Avaluació segons les caselles diagonals adjacent a les cantonades
     * @param board Estat actual del taulell
     * @param player Color del jugador
     * @return Diferencia dels X cells dels jugadors
     */
    private static int xSquares(Tauler board, Color player) {
        int minp = 0;
        int maxp = 0;

        if (board.getTauler()[1][1].getcolor() == player) minp += 1;
        if (board.getTauler()[1][6].getcolor() == player) minp += 1;
        if (board.getTauler()[6][1].getcolor() == player) minp += 1;
        if (board.getTauler()[6][6].getcolor() == player) minp += 1;

        if (board.getTauler()[1][1].getcolor() == oponent(player)) maxp += 1;
        if (board.getTauler()[1][6].getcolor() == oponent(player)) maxp += 1;
        if (board.getTauler()[6][1].getcolor() == oponent(player)) maxp += 1;
        if (board.getTauler()[6][6].getcolor() == oponent(player)) maxp += 1;

        return (minp - maxp);
    }

    /**
     * Avaluació segons els primers diagonals a l'inici de la partida
     * @param board Estat actual del taulell
     * @param player Color del jugador
     * @return Diferencia dels primers daigonals a l'inici de la partida dels jugadors
     */
    private static int firstDiagonal(Tauler board, Color player) {
        int minp = 0;
        int maxp = 0;

        if (board.getTauler()[2][2].getcolor() == player) minp += 1;
        if (board.getTauler()[2][5].getcolor() == player) minp += 1;
        if (board.getTauler()[5][2].getcolor() == player) minp += 1;
        if (board.getTauler()[5][5].getcolor() == player) minp += 1;

        if (board.getTauler()[2][2].getcolor() == oponent(player)) maxp += 1;
        if (board.getTauler()[2][5].getcolor() == oponent(player)) maxp += 1;
        if (board.getTauler()[5][2].getcolor() == oponent(player)) maxp += 1;
        if (board.getTauler()[5][5].getcolor() == oponent(player)) maxp += 1;

        return (minp - maxp);
    }

    /**
     * Avaluació segons dels laterals adjacents a les cantonades
     * @param board Estat actual del taulell
     * @param player Color del jugador
     * @return Diferencia dels lateras adjacents a les cantonades del jugadors
     */
    private static int cSquares(Tauler board, Color player){
        int minp = 0;
        int maxp = 0;

        if (board.getTauler()[0][1].getcolor() == player) minp += 1;
        if (board.getTauler()[1][0].getcolor() == player) minp += 1;
        if (board.getTauler()[0][6].getcolor() == player) minp += 1;
        if (board.getTauler()[1][7].getcolor() == player) minp += 1;
        if (board.getTauler()[6][0].getcolor() == player) minp += 1;
        if (board.getTauler()[7][1].getcolor() == player) minp += 1;
        if (board.getTauler()[6][7].getcolor() == player) minp += 1;
        if (board.getTauler()[7][6].getcolor() == player) minp += 1;

        if (board.getTauler()[0][1].getcolor() == oponent(player)) maxp += 1;
        if (board.getTauler()[1][0].getcolor() == oponent(player)) maxp += 1;
        if (board.getTauler()[0][6].getcolor() == oponent(player)) maxp += 1;
        if (board.getTauler()[1][7].getcolor() == oponent(player)) maxp += 1;
        if (board.getTauler()[6][0].getcolor() == oponent(player)) maxp += 1;
        if (board.getTauler()[7][1].getcolor() == oponent(player)) maxp += 1;
        if (board.getTauler()[6][7].getcolor() == oponent(player)) maxp += 1;
        if (board.getTauler()[7][6].getcolor() == oponent(player)) maxp += 1;


        return (minp - maxp);
    }

    /**
     * Avaluació segons el número de fitxes de cada jugador
     * @param board Estat actual del taulell
     * @param player Color del juagdor
     * @return Diferència de les fitxes dels jugadors
     */
    private static int evalDisc(Tauler board, Color player){
        int minp = 0;
        int maxp = 0;

        for(int i = 0; i < 8; i++){
            for (int j = 0; j < 8; j++){
                if (board.getTauler()[i][j].getcolor() == player) minp += 1;
                if (board.getTauler()[i][j].getcolor() == oponent(player)) maxp += 1;
            }
        }

        return (minp - maxp);
    }

    /**
     * Avaluació segons la mobilitat
     * @param board Estat actual del taulell
     * @param player Color del jugador
     * @return Diferència del número de moviments dels jugadors
     */
    private static int evalMobility(Tauler board, Color player){
        int minp = 0;
        int maxp = 0;

        for (int i = 0; i < 8; i++){
            for(int j = 0; j < 8; j++){
                Pair pos = new Pair(i, j);
                if (board.posicioValida(pos, player)) {
                    minp += 1;
                }
                if (board.posicioValida(pos, oponent(player))) {
                    maxp += 1;
                }
            }
        }
        return (minp - maxp);
    }

    /**
     * Avaluació segons les posicions de les fitxes (corners + x-squares + c-squares + first diagonal)
     * @param board Estat actual del taulell
     * @param player Color del jugador
     * @return Valor de l'estat segons la posició
     */
    private static int evalPosition(Tauler board, Color player){
        int a = corners(board, player);
        int b = xSquares(board, player);
        int c = cSquares(board, player);
        int d = firstDiagonal(board, player);

        return (1000 * a) + (-500 * b) + (-250 * c) + (100 * d);
    }

    /*private int evalParity(Tauler board, Color player){
        int total = board.getFitxesB() + board.getFitxesN();
        int remindRnd = 64 - total;
        return 0;
    }*/

    /**
     * Funció per saber en quina fase esta la partida
     * @param board Estat actual del taulell
     * @return Retorna la fase de la partida
     */
    private static gamePhase getFaseJoc(Tauler board){
        int n = board.getFitxesB() + board.getFitxesN();

        if (n < 20)  return gamePhase.earlyGame;
        else if (n < 50) return gamePhase.midGame;
        else return gamePhase.lateGame;
    }

    /**
     * Funció heurístic 1 estatic. Només es té en compte els pesos d'una matriu auxiliar
     * @param tauler Estat actual del taulell
     * @param player Color del jugador
     * @return Avaluacio heuristica del taulell
     */
    public static int heuristic1(Tauler tauler, Color player) {
        int value = 0;
        /*for (int i = 0; i < 8; i++){
            for(int j = 0; j < 8; j++){
                if (tauler.getTauler()[i][j].getcolor() == player) value  += pesos[i][j];
                if (tauler.getTauler()[i][j].getcolor() == oponenet(player)) value -= pesos[i][j];
            }
        }

        return value;*/
        boolean[][] visitat = new boolean[8][8];
        LinkedList<Pair> q = new LinkedList<>();
        q.add(new Pair(3,3));
        visitat[3][3] = true;
        Pair p;
        if (tauler.getTauler()[3][3].getcolor() == player) value  += pesos[3][3];
        if (tauler.getTauler()[3][3].getcolor() == oponent(player)) value -= pesos[3][3];
        while (!q.isEmpty()) {
            p = q.pop();
            for (Pair direccio : direccions) {
                int x = p.getKey() + direccio.getKey();
                int y = p.getValue() + direccio.getValue();
                Pair nou = new Pair(x, y);
                if (0 <= x && x <= 7 && 0 <= y && y <= 7 && !visitat[x][y]) {
                    visitat[x][y] = true;
                    Color col = tauler.getTauler()[x][y].getcolor();
                    if (col != Color.Buit) {
                        if (col == player) value += pesos[x][y];
                        else value -= pesos[x][y];
                        q.add(nou);
                    }
                }
            }
        }
        return value;
    }

    /**
     * Funció heurístic 2 dinamic. Es te diferents factors dins d'una partida segons la fase de joc
     * @param board Estat actual del taulell
     * @param player Color del jugador
     * @return Avaluació númeric del taulell board
     */
    public static int heuristic2(Tauler board, Color player) {

        if (board.isacabat()) return 100 * evalDisc(board, player);

        switch (getFaseJoc(board)){
            case earlyGame:
                return evalPosition(board, player) + 50 * evalMobility(board, player);
            case midGame:
                return evalPosition(board, player) + 25 * evalMobility(board, player) + 10 * evalDisc(board, player);
            case lateGame:
                return evalPosition(board, player) + 40 * evalMobility(board, player) + 60 * evalDisc(board, player);
        }
        return 0;
    }
}
