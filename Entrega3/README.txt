 Per a recompilar el programa hi ha un Makefile en el directori /FONTS/src. Disposa de les següents comandes:
 
 make -> compilar les classes
 
 make jar -> reconstruir el fitxer othello.jar al directori /EXE
 
 make clean -> esborrar els fitxers .class
 
 make reset -> esborrar les dades guardades del joc i reconstruir l'estructura de fitxers.
 
 
 
Les dades del joc es guarden al mateix directori que l'executable, ja que aquest les llegeix i les escriu. 
